﻿namespace InventoryApp.Services
{
  public interface IWebsiteContext
  {
    int WebsiteId { get; }
  }
}