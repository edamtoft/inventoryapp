﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InventoryApp.Models;
using Nest;

namespace InventoryApp.Services
{
  public sealed class DealerVehicleDatabase : IVehicleDatabase
  {
    private readonly IElasticClient _es;
    private readonly IWebsiteContext _websiteContext;

    public DealerVehicleDatabase(IElasticClient es, IWebsiteContext websiteContext)
    {
      _es = es;
      _websiteContext = websiteContext;
    }

    public async Task<FullVehicle> GetVehicle(string vin)
    {
      var documentId = $"{_websiteContext.WebsiteId}:{vin.ToUpper()}";
      var vehicleData = await _es.GetAsync<FullVehicle>(new GetRequest("inventory", "vehicle", documentId)
      {
        Routing = _websiteContext.WebsiteId.ToString()
      });

      if (!vehicleData.IsValid)
      {
        return null;
      }

      return vehicleData.Source;
    }
  }
}
