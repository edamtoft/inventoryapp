﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Services
{
  public sealed class WebsiteMiddleware
  {
    private readonly RequestDelegate _next;

    public WebsiteMiddleware(RequestDelegate next)
    {
      _next = next;
    }

    public async Task Invoke(HttpContext context, IWebsiteContext websiteContext, IWebsiteResolver resolver)
    {
      var dealerId = await resolver.ResolveWebsiteId(context);

      if (!dealerId.HasValue)
      {
        context.Response.StatusCode = 404;
        context.Response.ContentType = "text/plain";
        await context.Response.WriteAsync("Site Not Found");
      }

      if (websiteContext is WebsiteContext assignableContext)
      {
        assignableContext.WebsiteId = dealerId.Value;
      }
      else
      {
        throw new Exception($"Unable to assign DealerId. {nameof(IWebsiteContext)} should be registered as a scoped {nameof(WebsiteContext)}.");
      }

      await _next(context);
    }
  }
}
