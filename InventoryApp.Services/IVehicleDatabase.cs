﻿using InventoryApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Services
{
  public interface IVehicleDatabase
  {
    Task<FullVehicle> GetVehicle(string vin);
  }
}
