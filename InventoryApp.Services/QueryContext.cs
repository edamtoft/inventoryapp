﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Nest;

namespace InventoryApp.Services
{
  public class QueryContext : IQueryContext
  {
    private readonly IQueryCollection _query;

    public QueryContext(IHttpContextAccessor httpContext)
    {
      _query = httpContext.HttpContext.Request.Query;
    }

    public QueryContainer GetElasticsearchQuery()
    {
      return new BoolQuery { Must = GetQueries().Where(q => q != null).ToList() };
    }

    public int? GetPage()
    {
      return _query.TryGetValue("page", out var param) && int.TryParse(param, out var value)
        ? (int?)value
        : null;
    }

    public int? GetPerPage()
    {
      return _query.TryGetValue("perpage", out var param) && int.TryParse(param, out var value)
        ? (int?)value
        : null;
    }

    private IEnumerable<QueryContainer> GetQueries()
    {
      yield return GetQuery("year", Convert.ToInt32);
      yield return GetQuery("make");
      yield return GetQuery("model");
      yield return GetQuery("trim");
      yield return GetQuery("bodystyle");
    }

    private QueryContainer GetQuery(string key) => GetQuery(key, s => s);
    private QueryContainer GetQuery<T>(string key, Func<string,T> convert)
    {
      if (!_query.TryGetValue(key, out var strings) || strings.Count == 0)
      {
        return null;
      }

      var values = _query[key].First().Split('|');
      switch (values.Length)
      {
        case 0:
          return null;
        case 1:
          return new TermQuery { Field = key, Value = values.First() };
        default:
          return new TermsQuery { Field = key, Terms = values.ToList() };
      }
    }
  }
}
