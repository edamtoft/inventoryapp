﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using InventoryApp.Models;
using Nest;

namespace InventoryApp.Services
{
  public class InventorySearchContext : IInventorySearchContext
  {
    private readonly IQueryContext _queryContext;
    private readonly IElasticClient _es;
    private readonly IWebsiteContext _websiteContext;
    private readonly Lazy<Task<SearchData>> _searchData;

    public InventorySearchContext(IQueryContext queryContext, IElasticClient es, IWebsiteContext websiteContext)
    {
      _queryContext = queryContext;
      _es = es;
      _websiteContext = websiteContext;
      _searchData = new Lazy<Task<SearchData>>(LoadSearchData, isThreadSafe: true);
    }

    public Task<SearchData> GetSearchData() => _searchData.Value;
    private async Task<SearchData> LoadSearchData()
    {
      var filter = new List<QueryContainer>
      {
        new TermQuery { Field = "dealerId", Value = _websiteContext.WebsiteId },
        new TermQuery { Field = "type", Value = "u" }
      };

      filter.Add(_queryContext.GetElasticsearchQuery());

      var aggs = new AggregationDictionary();

      var aggregationTerms = new[] { "year", "make", "model", "trim" };

      foreach (var property in aggregationTerms)
      {
        var name = $"{property}-buckets";
        aggs.Add(name, new TermsAggregation(name)
        {
          Field = property
        });
      }

      var page = _queryContext.GetPage() ?? 1;
      var perPage = _queryContext.GetPerPage() ?? 10;

      var esResult = await _es.SearchAsync<Vehicle>(new SearchRequest("inventory", "vehicle")
      {
        Routing = new [] { _websiteContext.WebsiteId.ToString() },
        Sort = new List<ISort> { new SortField { Field = "price", Order = SortOrder.Ascending } },
        From = (page - 1) * perPage,
        Size = perPage,
        Query = new BoolQuery { Filter = filter },
        Aggregations = aggs,
        Source = new SourceFilter { Includes = VehicleFields.Basic.Values.ToArray() }
      });

      var model = new SearchData
      {
        Results = new SearchResults
        {
          Pages = new Pagination
          {
            Page = page,
            PerPage = perPage,
            Total = (int)esResult.Total,
          },
          Vehicles = esResult.Hits.Select(hit => hit.Source).ToList()
        },
        Refinements = new List<SearchRefinement>()
      };

      foreach (var property in aggregationTerms)
      {
        var name = $"{property}-buckets";
        model.Refinements.Add(new SearchRefinement
        {
          Field = property,
          Values = esResult
          .Aggregations
          .Terms(name)
          .Buckets
          .Select(bucket => new RefinementOption { Count = (int)bucket.DocCount, Value = bucket.Key.ToString() })
          .ToList()
        });
      }

      return model;
    }
  }
}
