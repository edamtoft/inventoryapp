﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Services
{
  public interface IQueryContext
  {
    QueryContainer GetElasticsearchQuery();
    int? GetPage();
    int? GetPerPage();
  }
}
