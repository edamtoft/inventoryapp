﻿using InventoryApp.Models;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace InventoryApp.Services
{
  internal static class VehicleFields
  {
    public static Dictionary<string,Field> Basic { get; } = typeof(Vehicle)
      .GetProperties(BindingFlags.Instance | BindingFlags.Public)
      .ToDictionary(prop => prop.Name, prop => new Field(prop));

    public static Dictionary<string, Field> Full { get; } = typeof(FullVehicle)
      .GetProperties(BindingFlags.Instance | BindingFlags.Public)
      .ToDictionary(prop => prop.Name, prop => new Field(prop));
  }
}
