﻿using InventoryApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Services
{
  public interface IInventorySearchContext
  {
    Task<SearchData> GetSearchData();
  }
}
