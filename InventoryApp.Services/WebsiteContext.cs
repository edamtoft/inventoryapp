﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Services
{
  public sealed class WebsiteContext : IWebsiteContext
  {
    public int WebsiteId { get; internal set; }
  }
}
