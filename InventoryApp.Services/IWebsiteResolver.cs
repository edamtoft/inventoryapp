﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace InventoryApp.Services
{
  public interface IWebsiteResolver
  {
    Task<int?> ResolveWebsiteId(HttpContext context);
  }
}