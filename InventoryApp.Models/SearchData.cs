﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Models
{
  public class SearchData
  {
    public SearchResults Results { get; set; }
    public List<SearchRefinement> Refinements { get; set; }
  }
}
