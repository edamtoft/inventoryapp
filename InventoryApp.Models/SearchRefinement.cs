﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Models
{
  public class SearchRefinement
  {
    public string Field { get; set; }
    public List<RefinementOption> Values { get; set; }
  }
}
