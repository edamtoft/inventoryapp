﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Models
{
  public class Pagination
  {
    public int Page { get; set; }
    public int PerPage { get; set; }
    public int Total { get; set; }

    public int PageCount => (int)Math.Ceiling(Total / (double)PerPage);
    public int FirstItem => (Page * PerPage) + 1;
    public int LastItem => Math.Min(Total, (Page * PerPage) + PerPage);
  }
}
