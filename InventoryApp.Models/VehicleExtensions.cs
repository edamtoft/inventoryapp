﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
  public static class VehicleExtensions
  {
    public static IEnumerable<string> Photos(this FullVehicle vehicle)
    {
      yield return vehicle.Photo;

      foreach (var photo in vehicle.VehiclePhotos)
      {
        yield return photo;
      }

      foreach (var photo in vehicle.StockPhotos)
      {
        yield return photo;
      }
    }
  }
}
