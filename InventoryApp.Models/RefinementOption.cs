﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Models
{
  public class RefinementOption
  {
    public string Value { get; set; }
    public int Count { get; set; }
  }
}
