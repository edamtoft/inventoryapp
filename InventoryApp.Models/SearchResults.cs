﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Models
{
  public class SearchResults
  {
    public Pagination Pages { get; set; }
    public List<Vehicle> Vehicles { get; set; }


  }
}
