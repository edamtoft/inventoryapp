﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace InventoryApp.Models
{
  public class Vehicle
  {
    public string Vin { get; set; }
    public double Price { get; set; }
    public int Year { get; set; }
    public string Make { get; set; }
    public string Model { get; set; }
    public string Trim { get; set; }
    public string Photo { get; set; }
    public string Bodystyle { get; set; }
    public string Name { get; set; }
    public int MpgCity { get; set; }
    public int MpgHwy { get; set; }
    public string StockNum { get; set; }
    public string Engine { get; set; }
    public string EngineCylinders { get; set; }
    public string EngineDisp { get; set; }
    public string ExteriorColor { get; set; }
    public int Mileage { get; set; }
    public string Transmission { get; set; }
  }
}
