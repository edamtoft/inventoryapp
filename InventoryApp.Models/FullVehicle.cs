﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace InventoryApp.Models
{
  public class FullVehicle : Vehicle
  {
    public string[] VehiclePhotos { get; set; }
    public string[] StockPhotos { get; set; }
    public string Comments { get; set; }
    public string Options { get; set; }
    public string TechSpecs { get; set; }
    public string Warranty { get; set; }
  }
}
