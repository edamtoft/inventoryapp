﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elasticsearch.Net;
using InventoryApp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;
using Nest;

namespace InventoryApp
{
  public class Startup
  {
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddScoped<IQueryContext, QueryContext>();
      services.AddScoped<IInventorySearchContext, InventorySearchContext>();
      services.AddScoped<IWebsiteResolver, WebsiteResolver>();
      services.AddScoped<IWebsiteContext, WebsiteContext>();
      services.AddScoped<IVehicleDatabase, DealerVehicleDatabase>();

      services.AddMvc();

      services.AddSingleton<IElasticClient>(c =>
      {
        var connectionPool = new SniffingConnectionPool(new[]
        {
          new Uri("http://esnode-1:9200"),
          new Uri("http://esnode-2:9200"),
          new Uri("http://esnode-3:9200"),
          new Uri("http://esnode-4:9200"),
          new Uri("http://esnode-5:9200"),
        });

        var settings = new ConnectionSettings(connectionPool);

        return new ElasticClient(settings);
      });
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseMiddleware<WebsiteMiddleware>();

      app.UseStaticFiles();

      app.UseMvc(options =>
      {
        options.MapRoute("default", "{controller=Home}/{action=Index}");
      });
    }
  }
}
