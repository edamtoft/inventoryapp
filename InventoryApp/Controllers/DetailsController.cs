﻿using InventoryApp.Models;
using InventoryApp.Services;
using Microsoft.AspNetCore.Mvc;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InventoryApp.Controllers
{
  public class DetailsController : Controller
  {
    private readonly IVehicleDatabase _vehicleDatabase;

    public DetailsController(IVehicleDatabase vehicleDatabase)
    {
      _vehicleDatabase = vehicleDatabase;
    }
    
    [HttpGet("[controller]/{vin}")]
    public async Task<IActionResult> Index(string vin)
    {
      return View("Index",await _vehicleDatabase.GetVehicle(vin));
    }
  }
}
