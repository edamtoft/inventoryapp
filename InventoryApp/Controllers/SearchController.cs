﻿using InventoryApp.Models;
using InventoryApp.Services;
using Microsoft.AspNetCore.Mvc;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InventoryApp.Controllers
{
  public class SearchController : Controller
  {
    private readonly IInventorySearchContext _searchContext;

    public SearchController(IInventorySearchContext searchContext)
    {
      _searchContext = searchContext;
    }
    
    [HttpGet]
    public async Task<IActionResult> Index()
    {
      return View("Index", await _searchContext.GetSearchData());
    }
  }
}
