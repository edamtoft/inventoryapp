﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.Views
{
  public static class VehicleSearchUrlExtensions
  {
    public static string WithTerm(this IUrlHelper url, string key, object value)
    {
      
      var req = url.ActionContext.HttpContext.Request;

      var queryString = req.Query
        .Where(q => !q.Key.Equals("page", StringComparison.OrdinalIgnoreCase))
        .Append(new KeyValuePair<string, StringValues>(key, new StringValues(value.ToString())))
        .OrderBy(q => q.Key);

      return req.Path.ToUriComponent() + QueryString.Create(queryString).ToUriComponent();
    }
  }
}
